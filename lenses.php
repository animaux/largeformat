<!DOCTYPE html>
<html lang="en">
 <head>
 <title>Large format lenses</title>
  
 <meta charset="utf-8">
 <meta name="desription" content="Specs for large format lenses">
 <meta name="robots" content="index">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 
 <script type="text/javascript" src="js/jquery-3.6.3.min.js"></script>
 <script type="text/javascript" src="js/datatables.min.js"></script>
 <script>
		$(document).ready(function() {
			
			var table = $('table').DataTable({
			  "columnDefs": [
					{ "type": "num", "targets": [3,4,5,6,8,9] }
				],
        "paging": false,
        // "responsive": true,
        "processing": true,
        "dom": 'fit'
			});
			
			// Spalten anzeigen/verstecken
			table.columns('.verbose').visible(false);
			let isVisible = true;
			$('#verbose').on( 'click', function (e) {
			  table.columns('.verbose').visible(isVisible);
			  isVisible = !isVisible;
			});
			
			// Formatfilter
			$('button.format').on('click', function () {
			  $.fn.dataTable.ext.search.pop()
			  $('button.format').removeClass('aktiv');
			  $(this).addClass('aktiv');
				var diagonal = $(this).attr('data-diagonal');
				$.fn.dataTable.ext.search.push(
					function(settings, data, dataIndex) {
						return parseInt(data[7]) >= diagonal ? true : false;
					}
				);
				table.draw();
			});
			
		});
 </script>
 
 <link href="css/screen.css" rel="stylesheet" media="screen" type="text/css" />
 
</head>

<body>

  <h1>
    <a href="../" title="back to index">←</a>
		<span>Large format lenses</span>
	</h1>
  
  <article class="full">
		<p>
			<a href="#imprint">↓ Imprint/Info below</a>
		  <span> · Click column headers to sort · </span>
			<button id="verbose">Toggle extra info</button>
		</p>
		<p>
			
			<span>Show lenses covering:</span>
			<button class="format" data-diagonal="150" title="≥ 150mm diagonal">9×12cm</button>
			<button class="format" data-diagonal="160" title="≥ 160mm diagonal">4×5"</button>
			<button class="format" data-diagonal="222" title="≥ 222mm diagonal">5×7"/13×18cm</button>
			<button class="format" data-diagonal="298" title="≥ 298mm diagonal">18×24cm</button>
			<button class="format" data-diagonal="324" title="≥ 324mm diagonal">8×10"</button>
			<button class="format" data-diagonal="453" title="≥ 453mm diagonal">11×14"</button>
			<button class="format" data-diagonal="560" title="≥ 560mm diagonal">14×17"</button>
			<!-- <button class="format" data-diagonal="650" title="≥ 650mm diagonal">16×20"</button> -->
		</p>
	</article>
  
  <table>
    <?php
      // don’t output rows
      $hidden = array(0);
      // rows with verbose info
      $verbose = array(6,7,14);
      
      $f = fopen("lenses.csv", "r");
      $index = 0;
      while (($line = fgetcsv($f)) !== false) {
        $celltype;
        if ($index == '0') { 
          echo "<thead>";
          $celltype = "th";
        } else {
          $celltype = "td";
        }
        // check if published
        if ($line[0]!='no') {
          echo "<tr>";
          $cindex;
          foreach ($line as $cell) {
            // don’t show hidden rows
            if (!in_array($cindex,  $hidden)) {
              echo "<" . $celltype;
              // is this row too verbose?
              if (in_array($cindex,  $verbose)) {
                echo " class='verbose";
                if ($cindex=='14') {
                  echo " cmt";
                }
                echo "'";
              }
              echo ">";
              // source
              if ($index != '0' && $cindex=='13') {
                $quellen = explode(",", $cell);
                foreach($quellen as $quelle) {
									echo ' <a href="#source' . $quelle . '">' . $quelle . '</a>';
								}
              } else {
                // normal cell content
                echo htmlspecialchars($cell);
              }
              // end of cell content
              echo "</" . $celltype . ">";
            }
            $cindex++;
          }
          $cindex = 0;
          echo "</tr>\n";
        }
        if ($index == '0') { echo "</thead><tbody>"; }
        $index++;
      }
      fclose($f);
      echo "</tbody>"
    ?>
  </table>
 
  <article>
    <p>All values in <b>millimeter</b>, except otherwise stated.</p>
    
    <h2 id="imprint">Imprint</h2>
		
		<p>Representation and visualisation of the data has been tinkered together by <a href="https://animaux.de">Alexander Rutz</a>.</p>
	
		<p>Feel free to contact me for errors, additions and contributions via <a href="mailto:ar@animaux.de">email</a> or <a href="https://gitlab.com/animaux/largeformat">GitLab</a> issues/pull requests.</p>
    
		<h3>Sources</h3>
		
		<ul>
		  <li><b>1–4.</b> Compiled by <a href="mailto:ab366@osfn.rhilinet.gov">Michael Gudzinowicz</a> for the <a href="http://largeformatphotography.info">Large Format Page</a>. Original <a href="http://www.largeformatphotography.info/lenseslist.html">here</a>.</li>
		  <li id="source1"><b>1.</b> <q cite="http://www.largeformatphotography.info/lenseslist.html">is a copy of a page of an equipment guide published in the mid 70’s which gives list prices. The exact reference has been lost (…)</q></li>
		  <li id="source2"><b>2.</b> <q cite="http://www.largeformatphotography.info/lenseslist.html">is a list published in Petersen’s in 1984 (list prices)</q></li>
		  <li id="source3"><b>3.</b> <q cite="http://www.largeformatphotography.info/lenseslist.html">is from the 1991 Calumet catalog (Calumet prices)</q></li>
		  <li id="source4"><b>4.</b> <q cite="http://www.largeformatphotography.info/lenseslist.html">is from the 1991 Sinar Bron catalog (Sinar prices)</q></li>
			<li id="source5"><b>5.</b> Compiled in January 2002 by <a href="mailto:zilch0@primenet.com">Michael K. Davis</a> for the <a href="http://largeformatphotography.info">Large Format Page</a>. Original <a href="http://www.largeformatphotography.info/lenses/">here</a>.</li>
			<li id="source6"><b>6.</b> Data from Jon Mcgloin’s <a href="http://www.subclub.org/fujinon/">Fujinon Large Format Lens List</a>. Used with kind permission by Jon Mcgloin. He states <q>(…) my information is not 100% correct. Fuji made many errors in their literature, and I can only correct it to a certain extent. Also several people have contacted me about Fujinon lenses that are not listed in their literature, but it is hard to tell if the lenses that they have are original or modified models.</q> For a lot more info please visit <a href="http://www.subclub.org/fujinon/">his site</a>.</li>
			<li id="source7"><b>7.</b> undated Computar Brochure by Burleigh Brooks Optics, Inc. and Burke &amp; James, Inc. For more Info see <a href="http://www.largeformatphotography.info/forum/showthread.php?10255-The-Computar-lens-and-ULF-coverage">this thread</a>, which goes into detail about different versions under different brand names and the source brochure.</li>
			<li id="source8"><b>8.</b> Schneider <a href="https://www.schneideroptics.com/">Website</a>.</li>
			<li id="source9"><b>9.</b> Cooke Optics <a href="https://www.cookeoptics.com/l/largeformat.html">Website</a>.</li>
			<li id="source10"><b>10.</b> Voigtländer Large Format Lenses from 1949–72, used with kind permission by Arne Cröll, 2005–14 <a href="http://www.arnecroell.com/voigtlaender.pdf">PDF</a> (1.6 MB).</li>
			<li id="source11"><b>11.</b> Reinhold Schable <a href="http://www.re-inventedphotoequip.com/Data.html">Website</a>.</li>
			<li id="source12"><b>12.</b> Yamasaki Optical Co. Ltd. <a href="http://www.jck.net/congo/index_e.html">Website</a>.</li>
			<li id="source13"><b>13.</b> Schneider Symmar 1:5,6 Broschure <a href="http://web.archive.org/web/20111011104748/http://schneiderkreuznach.com/archiv/pdf/symmar_5.6.pdf">PDF</a>.</li>
			<li id="source14"><b>14.</b> Large format lenses from VEB Carl Zeiss Jena 1945–1991, 2014–08-19 <a href="http://www.arnecroell.com/czj.pdf">PDF</a> (1.6 MB).</li>
			<li id="source15"><b>15.</b> Schneider Lenses Catalogue 1939 <a href="https://www.cameraeccentric.com/static/img/pdfs/schneider_2.pdf">PDF</a> (8.9 MB).</li>
			<li id="source16"><b>16.</b> Zeiss Objektive 1938 <a href="http://www.tontrennung.de/assets/documents/CarlZeiss/Prospekte/CZJ_1938_objektive.pdf">PDF</a> (1.8 MB).</li>
			<li id="source17"><b>17.</b> Kodak Lenses for Professional Photography 1951 <a href="https://www.pacificrimcamera.com/rl/00346/00346.pdf">PDF</a> (7.4 MB).</li>
			<li id="source18"><b>18.</b> Meyer Photo Lenses Catalogue 1936 <a href="https://www.cameraeccentric.com/static/img/pdfs/meyer_3.pdf">PDF</a> (8.1 MB).</li>
			<li id="source19"><b>19.</b> Hugo Meyer &amp; Co. Görlitz Reichsmark-Preisschlüssel 1939 <a href="http://ihagee.org/Lenzen/HMG6-1939Preisschluessel.pdf">PDF</a> (6.6 MB).</li>	
			<li id="source20"><b>20.</b> Voigtländer Lenses Catalogue 1927 <a href="https://www.cameraeccentric.com/static/img/pdfs/voigtlander_1.pdf">PDF</a> (15 MB).</li>
			
			<li id="source21"><b>21.</b> Rodenstock Data Sheet <a href="https://forum.grossformatfotografie.de/forum/thread/21674-rodenstock-eurygon-4-5-300mm/?postID=167151#post167151">Großformat Forum post</a>.</li>
			<!-- <li id="source21"><b>21.</b> Voigtländer Lenses Catalogue 1964 <a href="https://www.cameraeccentric.com/static/img/pdfs/voigtlander_2.pdf">PDF</a> (1.7 MB).</li> -->
			
			
			
		</ul>
		
		<h3>Disclaimer</h3>
		
		<p>This data is provided »as is«, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in this data with the software or the use or other dealings in this data.</p>
	</article>

</body>
</html>